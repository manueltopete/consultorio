using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Models;
using Repositorios;

namespace consulauth.Controllers
{
    public class ConsultorioController : Controller
    {
         private IConsultorioRepository consultorios;
         
         public ConsultorioController(IConsultorioRepository consulRepo)
        {
            consultorios = consulRepo;
        }
        // GET: Consultorio
        public async Task<ActionResult> Index()
        {
            
          
                 var model = from c in await consultorios.LeerTodos()
                 select new Consultorio(){
                 Id=c.Id,
                 Numero=c.Numero,
                 Identificador=c.Identificador
                 

                 };
            return View(model);
        }

        // GET: Consultorio/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Consultorio/Create
        public ActionResult Create()
        {
           var model = new CrearConsultorioModel();
            return View(model);
        }

        // POST: Consultorio/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
          public async  Task<ActionResult> Create(CrearConsultorioModel model)
        {
            if(ModelState.IsValid){
            try
            {
                // TODO: Add insert logic here
               await consultorios.Guardar(new ConsultorioIdentity(){
                Id=model.Id,
                Identificador=model.Identificador,
                Numero=model.Numero
                });
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
            }
            return View();
        }

        // GET: Consultorio/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Consultorio/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(EditarConsultorioModel model)
        {
            if(!ModelState.IsValid){
                return View(model);
            }
            try
            {
                // LLenar datos
                await consultorios.Actualizar(new ConsultorioIdentity(){
                Id=model.Id,
                Identificador=model.Identificador,
                Numero=model.Numero
                });
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Consultorio/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Consultorio/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
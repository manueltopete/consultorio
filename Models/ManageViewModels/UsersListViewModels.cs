using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace consulauth.Models.ManageViewModels
{

    

    public class UserInSystemViewModel{

         
        public String Email { get; set; }

        public String Roles { get; set; }

        public String UserName { get; set; }

    }

    public class CambiarRolViewModel
    {
        public CambiarRolViewModel()
        {
            RolesDisponibles= new List<String>();
        }
        public String UserName { get; set; }
        public String Email { get; set; }

        public String Roles { get; set; }

        public List<String> RolesDisponibles { get; private  set; } 
    }
}

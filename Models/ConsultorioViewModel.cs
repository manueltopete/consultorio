using System;
using System.ComponentModel.DataAnnotations;

namespace Models{
    public class Consultorio{

        public String Id { get; set; }
        public String Numero { get; set; }

        public String Identificador { get; set; }

    }

    public class CrearConsultorioModel
    {
        public String Id { get; set; }

        public String Numero { get; set; }

        public String Identificador { get; set; }

    }

    public class EditarConsultorioModel
   {

        public String Id { get; set; }

        public String Numero { get; set; }

        public String Identificador { get; set; }




   }
}
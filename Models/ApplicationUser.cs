﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace consulauth.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
        
       public String Especialidad { get; set; }
       
        public String Token { get;  set; }

        public String Cedula { get; set; }

  
    }
}

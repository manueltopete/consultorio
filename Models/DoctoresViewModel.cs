using System;
using System.ComponentModel.DataAnnotations;

namespace Models{
    public class Doctor{
        public String Id { get; set; }

        public String Nombre { get; set; }

        public String ApellidoP { get; set; }

        public String ApellidoM { get; set; }

        public String Especialidad { get; set; }

        public String Horario { get; set; }
        public String RFC { get; set; }

    }

    public class CrearDoctorModel
    {
        
        public String Nombre { get; set; }

        public String ApellidoP { get; set; }

        public String ApellidoM { get; set; }

        public String Especialidad { get; set; }


        public String Horario { get; set; }
        public String RFC { get; set; }

    }
     public class EditarDoctorModel{

         [UIHint("Hidden")]
        public String Id { get; set; }

        public String Nombre { get; set; }
        [Required]

        public String ApellidoP { get; set; }

        public String ApellidoM { get; set; }

        public String Especialidad { get; set; }

        public String Horario { get; set; }
         public String RFC { get; set; }

    }

    public class BorrarDoctorModel{

       
        public String Id { get; set; }

        public String Nombre { get; set; }
         
 
    }
}
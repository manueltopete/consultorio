﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using consulauth.Data;
using consulauth.Models;
using consulauth.Services;
using Repositorios;
using Microsoft.Extensions.Logging;
using authsample.Data;

namespace consulauth
{
    public class Startup
    {
       private IApplicationBuilder Application;

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlite(Configuration.GetConnectionString("DefaultConnection")));

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                
                .AddDefaultTokenProviders();
                services.Configure<IdentityOptions>( options =>{
                    options.Password.RequireDigit = false;
                    options.Password.RequiredLength = 4;
                    options.Password.RequireLowercase = false;
                    options.Password.RequireUppercase = false;
                    options.Password.RequireNonAlphanumeric = false;

                });

            // Add application services.
            services.AddTransient<IEmailSender, EmailSender>();

            services.AddMvc();
            var azConstring = Configuration.GetValue<string>("Doctores:azStorage");

            services.AddTransient<IDoctoresRepository, AzureDoctoresRepository>(
                    sr => 
                        new AzureDoctoresRepository(azConstring));
            services.AddTransient<IConsultorioRepository, AzureConsultorioRepository>(
                    sr2 => 
                        new AzureConsultorioRepository(azConstring));
        }
        ILoggerFactory loggerFactory;

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env
        ,UserManager<ApplicationUser> userManager
        ,RoleManager<IdentityRole> roleManager
        , ApplicationDbContext dbContext
        )
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }
          loggerFactory=app.ApplicationServices.GetService<ILoggerFactory>();


            app.UseStaticFiles();

            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
            dbContext.Database.EnsureCreated();
            IdentityInitializer.SeedRoles(roleManager);
             IdentityInitializer.SeedUsers(userManager);

        }
    }
}

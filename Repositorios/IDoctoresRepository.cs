using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Repositorios{

    public interface IDoctoresRepository
    {
        Task<DoctorEntity> LeerPorId(String id);

        Task<List<DoctorEntity>> LeerTodos();

        Task<bool>  Guardar(DoctorEntity nuevo);
        Task<bool>  Actualizar(DoctorEntity actualizado);
        Task<bool>  Borrar (String id);
    }
    public class DoctorEntity{
        public String Id { get; set; }

        public String Nombre { get; set; }

        public String ApellidoP { get; set; }

        public String ApellidoM { get; set; }

        public String Especialidad { get; set; }

        public String Horario { get; set; }
        public String RFC { get; set; }
    }


    
}
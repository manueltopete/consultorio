using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;

namespace Repositorios{

    public class AzureDoctoresRepository : IDoctoresRepository
    
    {
        private string conexion;

        public AzureDoctoresRepository(string azAccountConnection)
        {
            conexion = azAccountConnection;
        }

        private CloudTable ObtenerTablaAzure(){
            // Retrieve the storage account from the connection string.
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(conexion);

            // Create the table client.
            CloudTableClient tableClient = storageAccount.CreateCloudTableClient();

            // Retrieve a reference to the table.
            CloudTable table = tableClient.GetTableReference("Doctores");

            // Create the table if it doesn't exist.
           // table.CreateIfNotExistsAsync().GetAwaiter();
            return table;
        }
        public async Task<bool> Actualizar(DoctorEntity actualizado)
        {
            var table = ObtenerTablaAzure();
            // Create a retrieve operation that expects a customer entity.
            TableOperation retrieveOperation =   TableOperation.Retrieve<DoctorAzureEntity>(actualizado.Id.Substring(0,2), actualizado.Id);

            // Execute the operation.
            TableResult retrievedResult = await table.ExecuteAsync(retrieveOperation);

            // Assign the result to a CustomerEntity.
            var  updateEntity = retrievedResult.Result as DoctorAzureEntity;

            // Create the Delete TableOperation.
            if (updateEntity != null)
            {
                updateEntity.Nombre = actualizado.Nombre;
                updateEntity.ApellidoP = actualizado.ApellidoP;
                updateEntity.ApellidoM = actualizado.ApellidoM;
                updateEntity.Especialidad = actualizado.Especialidad;
                updateEntity.Horario = actualizado.Horario;
                 
               
                TableOperation updateOP = TableOperation.Replace(updateEntity);

                // Execute the operation.
               await table.ExecuteAsync(updateOP);
               return true;

                 
            }
            else
            {
                    return false;
            }
        }

        public async Task<bool>  Borrar(String id)
        {
            var table = ObtenerTablaAzure();
            // Create a retrieve operation that expects a customer entity.
            TableOperation retrieveOperation =   TableOperation.Retrieve<DoctorAzureEntity>(id.Substring(0,2), id);

            // Execute the operation.
            TableResult retrievedResult = await table.ExecuteAsync(retrieveOperation);

            // Assign the result to a CustomerEntity.
            var  deleteEntity = retrievedResult.Result as DoctorAzureEntity;

            // Create the Delete TableOperation.
            if (deleteEntity != null)
            {
                TableOperation deleteOperation = TableOperation.Delete(deleteEntity);

                // Execute the operation.
               await table.ExecuteAsync(deleteOperation);
               return true;

                 
            }
            else
            {
                    return false;
            }
        }

        public async Task<bool>  Guardar(DoctorEntity nuevo)
        {
            var table = ObtenerTablaAzure();

             var azE = new DoctorAzureEntity(nuevo.Nombre, nuevo.Id);
                azE.ApellidoP = nuevo.ApellidoP;
                azE.ApellidoM = nuevo.ApellidoM;  
                azE.Especialidad = nuevo.Especialidad;
                azE.Horario = nuevo.Horario;  


                var insertOperation = TableOperation.Insert(azE);
               await table.ExecuteAsync(insertOperation);
               return true;
        }

        public  async Task<DoctorEntity> LeerPorId(String id)
        {
            var table = ObtenerTablaAzure();

            TableOperation retrieveOperation = TableOperation.Retrieve<DoctorAzureEntity>(id.Substring(0,2),id);
            TableResult retrievedResult = await table.ExecuteAsync(retrieveOperation);

                // Print the phone number of the result.
                if (retrievedResult.Result != null)
                {
                    var r = retrievedResult.Result as DoctorAzureEntity;
                    return new DoctorEntity(){
                        Id = r.Id,
                        Nombre = r.Nombre,
                        ApellidoP = r.ApellidoP,
                        ApellidoM = r.ApellidoM,
                        Especialidad =r.Especialidad,
                        Horario = r.Horario

                    };
                 }
                else
                {
                    return null;
                 }
        }

        public async Task<List<DoctorEntity>> LeerTodos()
        {
            var table = ObtenerTablaAzure();
            var tk = new TableContinuationToken();
            var query = new TableQuery<DoctorAzureEntity>();
            var lista = new List<DoctorEntity>();
            do
            {
                // Retrieve a segment (up to 1,000 entities).
                TableQuerySegment<DoctorAzureEntity> tableQueryResult = await
                     table.ExecuteQuerySegmentedAsync(query, tk );

                // Assign the new continuation token to tell the service where to
                // continue on the next iteration (or null if it has reached the end).
                tk = tableQueryResult.ContinuationToken;
                lista.AddRange(tableQueryResult.Results.Select( az => new DoctorEntity(){
                    Id = az.Id,
                    Nombre = az.Nombre,
                    ApellidoP = az.ApellidoP,
                    ApellidoM = az.ApellidoM,
                    Especialidad = az.Especialidad,
                    Horario = az.Horario
                     })
                );

                // Print the number of rows retrieved.
 
            // Loop until a null continuation token is received, indicating the end of the table.
            } while(tk != null);
             return lista;

        }
    }


    public class DoctorAzureEntity : TableEntity
{
    public DoctorAzureEntity(string Nombre, string RFC)
    {
        this.PartitionKey = RFC.Substring(0,2);
        this.Nombre = Nombre;
        this.RowKey =RFC;
    }

    public DoctorAzureEntity(){}

        public String Id { get{
            return RowKey;
        }
        set {
            RowKey = value;
        }
        }

        public String Nombre { get; set; }

        public String ApellidoP { get; set; }

        public String ApellidoM { get; set; }

        public String Especialidad { get; set; }

        public String Horario { get; set; }

     }

}
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Repositorios{


     public interface IConsultorioRepository
    {
        Task<ConsultorioIdentity> LeerPorId(String id);

        Task<List<ConsultorioIdentity>> LeerTodos();

        Task<bool>  Guardar(ConsultorioIdentity nuevo);
        Task<bool>  Actualizar(ConsultorioIdentity actualizado);
        Task<bool>  Borrar (String id);
    }

public class ConsultorioIdentity{
         
       public String Id { get; set; }
        public String Numero { get; set; }

        public String Identificador { get; set; }
    }






}


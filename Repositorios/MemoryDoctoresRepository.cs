using System;
using System.Collections.Generic;
using System.Linq;

namespace Repositorios{
    public class MemoryDoctoresRepository 
    {
        private static List<DoctorEntity> db;

        static MemoryDoctoresRepository()
        {
            db= new List<DoctorEntity>();
            db.Add(new DoctorEntity() {
                Id="1",
                Nombre="Manuel",
                ApellidoP="Nuñez",
                ApellidoM="Topete",
                Especialidad="Cirujano Plastico",
                Horario="7:00am-12:00pm"

            });
        }
        public void Actualizar(DoctorEntity actualizado)
        {
            var existe=db.FirstOrDefault(c => c.Id== actualizado.Id);
            if(existe!= null){
                existe.Nombre=actualizado.Nombre;
                existe.ApellidoP=actualizado.ApellidoP;
                existe.ApellidoM=actualizado.ApellidoM;
                existe.Especialidad=actualizado.Especialidad;
                existe.Horario=actualizado.Horario;
            }
        }

        public void Borrar(String id)
        {
           var existe=db.FirstOrDefault(c => c.Id== id);
            if(existe!= null){
                db.Remove(existe);
            }
        }

        public void Guardar(DoctorEntity nuevo)
        {
      //      var id=0;
        //    if(db.Count>0){
          //      id=db.Max(c => c.Id) + 1;
            //}
            //nuevo.Id=id;
            db.Add(nuevo);
        }

        public DoctorEntity LeerPorId(String id)
        {
            return db.FirstOrDefault( c => c.Id == id);
        }

        public List<DoctorEntity> LeerTodos()
        {
            return db.ToList();
         }
    }
}
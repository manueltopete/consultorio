using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using MySql.Data;
using MySql;
using MySql.Data.MySqlClient;
using Microsoft.Extensions.Logging;

namespace Repositorios{
    public class MySqlDoctoresRepository  
    {
        string connStr;
        private  ILogger<MySqlDoctoresRepository> log;
        public MySqlDoctoresRepository(string connStrng ,ILogger<MySqlDoctoresRepository> logger)
        {
            connStr=connStrng;
            log=logger;
        }
       //  string connStr="server=localhost;user=root;database=doctores;port=3306;password=a";       
        public void Actualizar(DoctorEntity actualizado)
        {
           var cmd = new MySqlCommand("UPDATE doctores SET nombre=@nombre, apellidop=@apellidop, apellidom=@apellidom, especialidad=@especialidad, horario=@horario WHERE id=@id");
           cmd.Parameters.AddWithValue("@nombre",actualizado.Nombre);
           cmd.Parameters.AddWithValue("@apellidop",actualizado.ApellidoP);
           cmd.Parameters.AddWithValue("@apellidom",actualizado.ApellidoM);
           cmd.Parameters.AddWithValue("@especialidad",actualizado.Especialidad);
           cmd.Parameters.AddWithValue("@hoario",actualizado.Horario);
           cmd.Parameters.AddWithValue("@id",actualizado.Id);

        using( var conn = new MySqlConnection(connStr)){
             try{
                   cmd.Connection= conn;
                   cmd.Connection.Open();
                   cmd.ExecuteNonQuery();
                  

               }catch(Exception ex){
                   log.LogError(ex,"no se pudo hacer update");
               }
           }        }

        public void Borrar(String id)
        {
          var cmd = new MySqlCommand("DELETE FROM doctores WHERE id = @id");
           cmd.Parameters.AddWithValue("@id",id);
           using( var conn = new MySqlConnection(connStr)){
             try{
                   cmd.Connection= conn;
                   cmd.Connection.Open();
                   cmd.ExecuteNonQuery();
                  

               }catch(Exception ex){
     log.LogError(ex,"no se pudo hacer delete");
 
               }
           }        }

        public void Guardar(DoctorEntity nuevo)
        {
           var cmd = new MySqlCommand("INSERT into doctores (nombre, apellidop, apellidom, especialidad, horario) values (@nombre, @apellidop, @apellidom, @especialidad, @horario)");
           cmd.Parameters.AddWithValue("@nombre",nuevo.Nombre);
           cmd.Parameters.AddWithValue("@apellidop",nuevo.ApellidoP);
           cmd.Parameters.AddWithValue("@apellidom",nuevo.ApellidoM);
           cmd.Parameters.AddWithValue("@especialidad",nuevo.Especialidad);
           cmd.Parameters.AddWithValue("@hoario",nuevo.Horario);
        using( var conn = new MySqlConnection(connStr)){
             try{
                   cmd.Connection= conn;
                   cmd.Connection.Open();
                   cmd.ExecuteNonQuery();
                  

               }catch(Exception ex){
                   log.LogError(ex,"no se pudo hacer inseert");
               }
           }

        }

        public DoctorEntity LeerPorId(String id)
        {
            
            var cmd= new MySqlCommand("SELECT id, nombre, apellidop, apellidom, especialidad, horario FROM doctores WHERE id= @id");
            cmd.Parameters.AddWithValue("@id",id);
            using( var conn = new MySqlConnection(connStr)){
               try{
                   cmd.Connection= conn;
                   cmd.Connection.Open();
                   using(var reader = cmd.ExecuteReader()){
                       if(reader.Read())
                        {
                            return ParseDoctor(reader);
                        }
                    }

               }catch(Exception ex){
                   log.LogError(ex,"no se pudo hacer select");
               }
           }
           return null;
        }

        private static DoctorEntity ParseDoctor(MySqlDataReader reader)
        {
            var i = 0;
            var c = new DoctorEntity();
            c.Id = reader.GetString(i++);
            c.Nombre = reader.GetString(i++);
            c.ApellidoP = reader.GetString(i++);
            c.ApellidoM = reader.GetString(i++);
            c.Especialidad = reader.GetString(i++);
            c.Horario = reader.GetString(i++);
            return c;
        }

        public List<DoctorEntity> LeerTodos()
        {
            var lista= new List<DoctorEntity>();
            var cmd= new MySqlCommand("SELECT id, nombre, apellidop, apellidom, especialidad, horario FROM doctores");
            using( var conn = new MySqlConnection(connStr)){
               try{
                   cmd.Connection= conn;
                   cmd.Connection.Open();
                   using(var reader = cmd.ExecuteReader()){
                       while(reader.Read()){
                      lista.Add(ParseDoctor(reader));
                        }
                   }

               }catch(Exception ex){
                   log.LogError(ex,"no se pudo hacer select");
               }
           }
           return lista;
           
            
        }
    }
}